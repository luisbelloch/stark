FROM python:3.6.4
LABEL maintainer="Luis Belloch <luis@luisbelloch.es>"

WORKDIR /opt/stark

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY identifier ./identifier/
COPY version ./

EXPOSE 80

CMD ["gunicorn", "--workers", "3", "-b", ":80", "identifier.app"]

