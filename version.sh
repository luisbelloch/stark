#!/bin/bash
if [[ -z "${CI_PIPELINE_ID}" ]]; then
    echo 0
else
    echo "${CI_PIPELINE_ID}.${CI_COMMIT_REF_SLUG}.${CI_COMMIT_SHA:0:6}"
fi

