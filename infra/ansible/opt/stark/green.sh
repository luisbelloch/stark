#!/bin/bash
set -eou pipefail
ln -fs /etc/nginx/sites-available/green.conf /etc/nginx/sites-enabled/default
service nginx restart

