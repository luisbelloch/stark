#!/bin/bash
set -eou pipefail
ln -fs /etc/nginx/sites-available/blue.conf /etc/nginx/sites-enabled/default
service nginx restart

