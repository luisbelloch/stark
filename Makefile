.PHONY: clean docker-build docker-run gitlab-test server test watch-prod watch-uat

VENV = .venv
RUNNER = .bin/gitlab-runner
VERSION = ./version

server:
	gunicorn --reload identifier.app

${VENV}:
	python3 -m venv $@
	$@/bin/pip install --upgrade -r requirements.txt

test: ${VENV}
	${VENV}/bin/pytest -v

${VERSION}:
	./version.sh > version

${RUNNER}:
	mkdir -p .bin
	curl --output ${RUNNER} https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
	chmod +x ${RUNNER}

gitlab-test: ${RUNNER}
	${RUNNER} exec docker test

docker-build: ${VERSION}
	docker build -t luisbelloch/stark .

docker-run: docker-build
	docker run -p 8000:80 -ti luisbelloch/stark

watch-prod:
	watch --color 'curl -s prod.luisbelloch.es/v1/identifier | jq -cC "{api,version}"'

watch-uat:
	watch --color 'curl -s uat.luisbelloch.es/v1/identifier | jq -cC "{api,version}"'

clean:
	rm -rf .bin ${VENV} ${VERSION}

