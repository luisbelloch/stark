from os import path

def version():
  if not path.exists('./version'):
    return '0'
  with open('./version', 'r') as f:
   return f.readline().strip()
  
