from identifier.app import random_identifier

def test_identifiers_are_6_characters_long():
  identifier = random_identifier()
  assert len(identifier) == 6

