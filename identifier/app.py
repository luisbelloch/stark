import falcon

from datetime import datetime
from os import getenv
from random import randint

from .version import version

current_version = version()

def random_identifier() -> str:
  return hex(randint(0x111111, 0xFFFFFF))[2:]
    
class IdentifierResourceV1:
  def on_get(self, req, res):
    res.media = { 
      'api': 'v1',
      'now': datetime.now().timestamp(),
      'version': current_version,
      'identifier': random_identifier()
    }

api = application = falcon.API()
api.add_route('/v1/identifier', IdentifierResourceV1())

